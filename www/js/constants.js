angular.module('instagrom.constants', [])
    .constant('URL', {
        base: 'https://lit-earth-2880.herokuapp.com',
        authenticate: '/api/user/authenticate',

        post: '/api/post',

        postFeeds: '/api/post/feeds',
        postLike: '/api/like/post',
        postComment: '/api/comment/post',
        postAll: '/api/post/all',
        postUser: '/api/post/user',

        user: '/api/user',
        userFollow: '/api/user/follow',
        userSearch: '/api/user/search',
        userFollower: '/api/user/followers',
        userFollowee: '/api/user/followees',

        userActivity: '/api/user/activity',
    });
