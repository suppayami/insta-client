// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('instagrom', ['ionic', 'instagrom.controllers', 'instagrom.services', 'ngCordova'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleLightContent();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.navBar.alignTitle('center');

  $stateProvider

    .state('login', {
      url: '/login',
      cache: false,
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
    })

    .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'templates/tabs.html'
    })

    .state('tab.home', {
      url: '/home',
      cache: false,
      views: {
        'tab-home': {
          templateUrl: 'templates/tab-home.html',
          controller: 'HomeCtrl'
        }
      }
    })

    .state('tab.discover', {
      url: '/discover',
      cache: false,
      views: {
        'tab-discover': {
          templateUrl: 'templates/tab-discover.html',
          controller: 'DiscoverCtrl'
        }
      }
    })

    .state('tab.search', {
      url: '/search',
      // cache: false,
      views: {
        'tab-discover': {
          templateUrl: 'templates/tab-search.html',
          controller: 'SearchCtrl'
        }
      }
    })

    .state('tab.camera', {
      url: '/camera',
      cache: false,
      views: {
        'tab-camera': {
          templateUrl: 'templates/tab-camera.html',
          controller: 'CameraCtrl'
        }
      }
    })

    .state('tab.activity', {
      url: '/activity',
      cache: false,
      views: {
        'tab-activity': {
          templateUrl: 'templates/tab-activity.html',
          controller: 'ActivityCtrl'
        }
      }
    })

    .state('tab.profile', {
      url: '/profile',
      cache: false,
      views: {
        'tab-profile': {
          templateUrl: 'templates/tab-profile.html',
          controller: 'ProfileCtrl'
        }
      }
    })

    .state('tab.home-comment', {
      url: '/home-comment/:id',
      cache: false,
      views: {
        'tab-home': {
          templateUrl: 'templates/tab-comment.html',
          controller: 'CommentCtrl',
        }
      }
    })

    .state('tab.discover-comment', {
      url: '/discover-comment/:id',
      cache: false,
      views: {
        'tab-discover': {
          templateUrl: 'templates/tab-comment.html',
          controller: 'CommentCtrl',
        }
      }
    })

    .state('tab.profile-comment', {
      url: '/profile-comment/:id',
      cache: false,
      views: {
        'tab-profile': {
          templateUrl: 'templates/tab-comment.html',
          controller: 'CommentCtrl',
        }
      }
    })

    .state('tab.home-profile-comment', {
      url: '/home-profile-comment/:id',
      cache: false,
      views: {
        'tab-home': {
          templateUrl: 'templates/tab-comment.html',
          controller: 'CommentCtrl',
        }
      }
    })

    .state('tab.discover-profile-comment', {
      url: '/discover-profile-comment/:id',
      cache: false,
      views: {
        'tab-discover': {
          templateUrl: 'templates/tab-comment.html',
          controller: 'CommentCtrl',
        }
      }
    })

    .state('tab.home-profile', {
      url: '/home-profile/:id',
      cache: false,
      views: {
        'tab-home': {
          templateUrl: 'templates/tab-profile.html',
          controller: 'ProfileCtrl'
        }
      }
    })

    .state('tab.discover-profile', {
      url: '/discover-profile/:id',
      cache: false,
      views: {
        'tab-discover': {
          templateUrl: 'templates/tab-profile.html',
          controller: 'ProfileCtrl'
        }
      }
    })

    .state('tab.profile-profile', {
      url: '/profile-profile/:id',
      cache: false,
      views: {
        'tab-profile': {
          templateUrl: 'templates/tab-profile.html',
          controller: 'ProfileCtrl'
        }
      }
    })

    .state('tab.profile-follower', {
      url: '/profile-follower/:id',
      cache: false,
      views: {
        'tab-profile': {
          templateUrl: 'templates/tab-follower.html',
          controller: 'FollowerCtrl'
        }
      },
      data: {
        r: 'follower'
      }
    })

    .state('tab.profile-followee', {
      url: '/profile-followee/:id',
      cache: false,
      views: {
        'tab-profile': {
          templateUrl: 'templates/tab-follower.html',
          controller: 'FollowerCtrl'
        }
      },
      data: {
        r: 'followee'
      }
    })

    .state('tab.discover-profile-followee', {
      url: '/discover-profile-followee/:id',
      cache: false,
      views: {
        'tab-discover': {
          templateUrl: 'templates/tab-follower.html',
          controller: 'FollowerCtrl'
        }
      },
      data: {
        r: 'followee'
      }
    })

    .state('tab.discover-profile-follower', {
      url: '/discover-profile-follower/:id',
      cache: false,
      views: {
        'tab-discover': {
          templateUrl: 'templates/tab-follower.html',
          controller: 'FollowerCtrl'
        }
      },
      data: {
        r: 'follower'
      }
    })

    .state('tab.home-profile-follower', {
      url: '/home-profile-follower/:id',
      cache: false,
      views: {
        'tab-home': {
          templateUrl: 'templates/tab-follower.html',
          controller: 'FollowerCtrl'
        }
      },
      data: {
        r: 'follower'
      }
    })

    .state('tab.home-profile-followee', {
      url: '/home-profile-followee/:id',
      cache: false,
      views: {
        'tab-home': {
          templateUrl: 'templates/tab-follower.html',
          controller: 'FollowerCtrl'
        }
      },
      data: {
        r: 'followee'
      }
    });

  $urlRouterProvider.otherwise('/login');
});
