angular.module('instagrom.services', ['ionic', 'instagrom.constants'])
    .factory('AuthService', function($q, $http, $ionicPopup, URL) {
        var LOCAL_TOKEN_KEY = 'instogromToken',
            LOCAL_USER_KEY  = 'instogromUser';

        var getAuthenticate = function(data) {
            return $http.get(URL.base + URL.authenticate, {
                method: 'GET',
                params: data
            });
        };

        var postAuthenticate = function(data) {
            return $http.post(URL.base + URL.authenticate, data);
        };

        var postUser = function(data) {
            return $http.post(URL.base + URL.user, data);
        };

        var destroyUserToken = function() {
            this.user = undefined;
            $http.defaults.headers.common['x-auth-token'] = undefined;
            window.localStorage.removeItem(LOCAL_TOKEN_KEY);
            window.localStorage.removeItem(LOCAL_USER_KEY);
        };

        var useToken = function(token, usr) {
            $http.defaults.headers.common['x-auth-token'] = token;
            this.user = JSON.parse(usr);
            window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
            window.localStorage.setItem(LOCAL_USER_KEY, usr);
        };

        var storeUserToken = function(token, usr) {
            useToken(token, usr);
        };

        var loadUserToken = function() {
            var token = window.localStorage.getItem(LOCAL_TOKEN_KEY),
                usr  = window.localStorage.getItem(LOCAL_USER_KEY);

            if (!!token) {
                useToken(token, usr);
            }
        };

        var login = function(data) {
            return $q(function(resolve, reject) {
                postAuthenticate(data)
                    .success(function(res) {
                        storeUserToken(res.token, JSON.stringify(res.user));
                        resolve(res);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };

        var logout = function() {
            destroyUserToken();

            return $ionicPopup.alert({
                title: 'Logged out!',
                template: 'Your login session is ended.'
            });
        };

        var signup = function(data) {
            return $q(function(resolve, reject) {
                postUser(data)
                    .success(function(res) {
                        resolve(res);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };

        loadUserToken();

        return {
            getAuthenticate: getAuthenticate,
            destroyUserToken: destroyUserToken,
            login: login,
            logout: logout,
            signup: signup,

            user: this.user
        };
    })

    .factory('API', function($rootScope, $q, $http, $ionicLoading, URL) {
        $rootScope.show = function(text) {
            $rootScope.loading = $ionicLoading.show({
                template: text ? text : 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
        };

        $rootScope.hide = function() {
            $ionicLoading.hide();
        };

        var getPostFeeds = function(data) {
            return $http.get(URL.base + URL.postFeeds, {
                method: 'GET',
                params: data
            });
        };

        var putPostLike = function(id) {
            return $http.put(URL.base + URL.postLike + "/" + id);
        };

        var getPostComment = function(id) {
            return $http.get(URL.base + URL.postComment + "/" + id);
        };

        var postPostComment = function(id, data) {
            return $http.post(URL.base + URL.postComment + "/" + id, data);
        };

        var getPostAll = function() {
            return $http.get(URL.base + URL.postAll);
        };

        var getPostUser = function(id) {
            return $http.get(URL.base + URL.postUser + "/" + id);
        };

        var getUser = function(id) {
            return $http.get(URL.base + URL.user + "/" + id);
        };

        var getUserFollow = function(id) {
            return $http.get(URL.base + URL.userFollow + "/" + id);
        };

        var putUserFollow = function(id) {
            return $http.put(URL.base + URL.userFollow + "/" + id);
        };

        var postUserSearch = function(data) {
            return $http.post(URL.base + URL.userSearch, data);
        };

        var getUserFollower = function(id) {
            return $http.get(URL.base + URL.userFollower + "/" + id);
        };

        var getUserFollowee = function(id) {
            return $http.get(URL.base + URL.userFollowee + "/" + id);
        };

        var getUserActivity = function() {
            return $http.get(URL.base + URL.userActivity);
        };

        var postPost = function(data) {
            return $http.post(URL.base + URL.post, data);
        };

        return {
            getPostFeeds: getPostFeeds,
            putPostLike: putPostLike,
            getPostComment: getPostComment,
            postPostComment: postPostComment,
            getPostAll: getPostAll,
            getPostUser: getPostUser,

            getUser: getUser,
            getUserFollow: getUserFollow,
            putUserFollow: putUserFollow,
            postUserSearch: postUserSearch,

            getUserFollower: getUserFollower,
            getUserFollowee: getUserFollowee,

            getUserActivity: getUserActivity,

            postPost: postPost
        };
    })

    .service('Profiles', function($http, $q, $cordovaFileTransfer, AuthService, API, URL) {
        var that = this;

        this.profile = null;
        this.following = {};

        this.isFollowing = function(id) {
            return $q(function(resolve, reject) {
                API.getUserFollow(id)
                    .success(function(res) {
                        that.following[id] = res.isFollow;
                        resolve(that.following[id]);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };

        this.toggleFollow = function(id) {
            return $q(function(resolve, reject) {
                API.putUserFollow(id)
                    .success(function(res) {
                        that.following[id] = !that.following[id];
                        resolve(that.following[id]);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };

        this.searchUser = function(data) {
            return $q(function(resolve, reject) {
                API.postUserSearch(data)
                    .success(function(res) {
                        resolve(res.users);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };

        this.putAvatar = function(path) {
            var options = {
                fileKey: 'image',
                httpMethod: 'put',
                headers: {
                    "x-auth-token": $http.defaults.headers.common['x-auth-token']
                }
            };

            return $q(function(resolve, reject) {
                $cordovaFileTransfer.upload(encodeURI(URL.base + URL.user), path, options, true)
                    .then(function(res) {
                        resolve(res);
                    }, function(err) {
                        reject(err);
                    }, function(progress) {

                    });
            });
        };

        this.refreshProfile = function(id) {
            return $q(function(resolve, reject) {
                API.getUser(id)
                    .success(function(res) {
                        that.profile = res.user;
                        resolve(that.profile);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };

        this.refreshFollower = function(id) {
            return $q(function(resolve, reject) {
                API.getUserFollower(id)
                    .success(function(res) {
                        resolve(res);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };

        this.refreshFollowee = function(id) {
            return $q(function(resolve, reject) {
                API.getUserFollowee(id)
                    .success(function(res) {
                        resolve(res);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };

        this.refreshActivity = function() {
            return $q(function(resolve, reject) {
                API.getUserActivity()
                    .success(function(res) {
                        resolve(res);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };
    })

    .service('Posts', function($q, $http, AuthService, API, URL, $cordovaFileTransfer) {
        var that = this;

        this.newfeeds = [];
        this.discover = [];
        this.profile  = {};

        this.findPostById = function(id) {
            var allPosts = that.newfeeds.concat(that.discover);
            var result = [];

            for (var key in that.profile) {
                allPosts = allPosts.concat(that.profile[key]);
            }

            for (var i = 0; i < allPosts.length; i++) {
                if (allPosts[i]._id === id) {
                    result.push(allPosts[i]);
                }
            }

            return result;
        };

        this.toggleLike = function(id) {
            var posts = that.findPostById(id);
            var user = AuthService.user;

            for (var i = 0; i < posts.length; i++) {
                if (posts[i].likes.indexOf(user._id) > -1) {
                    var index = posts[i].likes.indexOf(user._id);

                    posts[i].likes.splice(index, 1);
                    posts[i].likeCount -= 1;
                } else {
                    posts[i].likes.push(user._id);
                    posts[i].likeCount += 1;
                }
            }

            API.putPostLike(id)
                .success(function(res) {
                    //
                })

                .error(function(res) {
                    //
                });
        };

        this.checkLike = function(id) {
            if (!AuthService.user) {
                return false;
            }

            var posts = that.findPostById(id);
            var user = AuthService.user;

            for (var i = 0; i < posts.length; i++) {
                if (posts[i].likes.indexOf(user._id) > -1) {
                    return true;
                }

                return false;
            }

            return false;
        };

        this.postComment = function(id) {
            var posts = that.findPostById(id);

            for (var i = 0; i < posts.length; i++) {
                posts[i].commentCount += 1;
            }
        };

        this.postPost = function(path, data) {
            var options = {
                fileKey: 'image',
                params: data,
                headers: {
                    "x-auth-token": $http.defaults.headers.common['x-auth-token']
                }
            };

            return $q(function(resolve, reject) {
                $cordovaFileTransfer.upload(encodeURI(URL.base + URL.post), path, options, true)
                    .then(function(res) {
                        resolve(res);
                    }, function(err) {
                        reject(err);
                    }, function(progress) {

                    });
            });
        };

        this.refreshNewfeeds = function() {
            return $q(function(resolve, reject) {
                API.getPostFeeds()
                    .success(function(res) {
                        that.newfeeds = res;
                        resolve(that.newfeeds);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };

        this.refreshDiscover = function() {
            return $q(function(resolve, reject) {
                API.getPostAll()
                    .success(function(res) {
                        that.discover = res;
                        resolve(that.discover);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };

        this.refreshProfile = function(id) {
            return $q(function(resolve, reject) {
                API.getPostUser(id)
                    .success(function(res) {
                        that.profile[id] = res;
                        resolve(that.profile[id]);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };
    })

    .service('Comments', function($q, Posts, AuthService, API) {
        var that = this;

        this.comments = {};

        this.refreshComments = function(id) {
            return $q(function(resolve, reject) {
                API.getPostComment(id)
                    .success(function(res) {
                        that.comments[id] = res;
                        resolve(that.comments[id]);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };

        this.postComment = function(id, data) {
            return $q(function(resolve, reject) {
                API.postPostComment(id, data)
                    .success(function(res) {
                        Posts.postComment(id);
                        that.comments[id].unshift(res.comment);
                        resolve(res.comment);
                    })

                    .error(function(err) {
                        reject(err);
                    });
            });
        };
    });
