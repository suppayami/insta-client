angular.module('instagrom.controllers', ['instagrom.services', 'angularMoment'])

    .controller('AppCtrl', function($rootScope, $state, $stateParams, AuthService, API) {
        $rootScope.show('Please wait...');

        AuthService.getAuthenticate()
            .success(function(res) {
                AuthService.user = res.user;
                $rootScope.hide();
                $state.go('tab.home', {}, {reload: true});
            })

            .error(function(err) {
                AuthService.destroyUserToken();
                $rootScope.hide();
                $state.go('login', {}, {reload: true});
            });
    })

    .controller('HomeCtrl', function($rootScope, $scope, Posts) {
        $rootScope.show('Please wait...');

        $scope.checkLiked = function(id) {
            return Posts.checkLike(id);
        };

        $scope.like = function(id) {
            Posts.toggleLike(id);
        };

        $scope.refresh = function() {
            Posts.refreshNewfeeds()
                .then(function(res) {
                    $rootScope.hide();
                    $scope.newfeeds = res;
                    $scope.$broadcast('scroll.refreshComplete');
                }, function(err) {
                    $rootScope.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };

        $scope.refresh();
    }) // HomeCtrl

    .controller('SearchCtrl', function($rootScope, $scope, Profiles, AuthService) {
        $scope.search = {
            searchKey: ""
        };

        $scope.users = [];

        $scope.toggleFollow = function(id) {
            Profiles.toggleFollow(id)
                .then(function(res) {

                }, function(err) {

                });
        };

        $scope.isFollowing = function(id) {
            return !!Profiles.following[id];
        };

        $scope.notCurrentUser = function(id) {
            return AuthService.user._id !== id;
        };

        $scope.searchUser = function() {
            $rootScope.show('Please wait...');

            Profiles.searchUser($scope.search)
                .then(function(res) {
                    $rootScope.hide();
                    $scope.users = res;

                    for (var i = 0; i < res.length; i++) {
                        Profiles.isFollowing(res[i]._id)
                            .then(function(res) {
                                //
                            }, function(err) {
                                //
                            });
                    }
                }, function(err) {
                    $rootScope.hide();
                });
        };
    }) // SearchCtrl

    .controller('DiscoverCtrl', function($rootScope, $scope, Posts) {
        $rootScope.show('Please wait...');

        $scope.checkLiked = function(id) {
            return Posts.checkLike(id);
        };

        $scope.like = function(id) {
            Posts.toggleLike(id);
        };

        $scope.refresh = function() {
            Posts.refreshDiscover()
                .then(function(res) {
                    $rootScope.hide();
                    $scope.gallery = res;
                    $scope.$broadcast('scroll.refreshComplete');
                }, function(err) {
                    $rootScope.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };

        $scope.refresh();
    }) // DiscoverCtrl

    .controller('CameraCtrl', function($state, $rootScope, $scope, $ionicPlatform, $ionicPopup, $cordovaCamera, $cordovaImagePicker, Posts) {
        $scope.img = "";
        $scope.data = {};

        $scope.isPick = function() {
            return $scope.img !== "";
        };

        $scope.picker = function() {
            var options = {
                maximumImagesCount: 1,
                width: 800,
                height: 0,
                quality: 80
            };

            $cordovaImagePicker.getPictures(options)
                .then(function(results) {
                    $scope.img = results[0];
                }, function(err) {
                    $ionicPopup.alert({
                        title: 'Image picking failure',
                        template: 'Failed on picking an image to post.'
                    });
                });
        };

        $scope.camera = function() {
            var options = {
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.CAMERA
            };

            $cordovaCamera.getPicture(options).then(function(imageURI) {
                $scope.img = imageURI;
            }, function(err) {
                $ionicPopup.alert({
                    title: 'Capture failure',
                    template: 'Failed on capture an image to post.'
                });
            });
        };

        $scope.post = function() {
            $rootScope.show('Please wait...');

            Posts.postPost($scope.img, $scope.data)
                .then(function(res) {
                    $rootScope.hide();
                    $state.go('tab.home', {}, {reload: true});
                }, function(err) {
                    $rootScope.hide();
                    $ionicPopup.alert({
                        title: 'Post failure',
                        template: 'There were some problems with posting'
                    });
                });
        };
    }) // SearchCtrl

    .controller('ActivityCtrl', function($scope, $rootScope, Profiles, AuthService) {
        $rootScope.show('Please wait...');

        $scope.getMessage = function(activity) {
            if (activity.type === "follow") {
                return "has followed you.";
            }

            if (activity.type === "like") {
                return "has liked your photo.";
            }

            if (activity.type === "comment") {
                return "has commented on your photo.";
            }

            return "";
        };

        $scope.refresh = function() {
            Profiles.refreshActivity()
                .then(function(res) {
                    $rootScope.hide();
                    $scope.activities = res;
                    $scope.$broadcast('scroll.refreshComplete');
                }, function(err) {
                    $rootScope.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };

        $scope.refresh();
    }) // ActivityCtrl

    .controller('ProfileCtrl', function($state, $stateParams, $rootScope, $scope, $cordovaImagePicker, $ionicPopup, Posts, Profiles, AuthService) {
        $rootScope.show('Please wait...');

        $scope.checkLiked = function(id) {
            return Posts.checkLike(id);
        };

        $scope.like = function(id) {
            Posts.toggleLike(id);
        };

        $scope.toggleFollow = function(id) {
            Profiles.toggleFollow(id)
                .then(function(res) {

                }, function(err) {

                });
        };

        $scope.isFollowing = function(id) {
            return Profiles.following[id];
        };

        $scope.signOut = function() {
            AuthService.destroyUserToken();
            $state.go('login', {}, {reload: true});
        };

        $scope.changeAvatar = function() {
            var options = {
                maximumImagesCount: 1,
                width: 800,
                height: 0,
                quality: 80
            };

            if ($scope.notCurrentUser) return false;

            $cordovaImagePicker.getPictures(options)
                .then(function(results) {
                    if (!results[0]) {
                        return false;
                    }

                    $rootScope.show("Changing avatar...");

                    Profiles.putAvatar(results[0])
                        .then(function(res) {
                            $rootScope.hide();
                            $scope.refresh();
                        }, function(err) {
                            $rootScope.hide();
                            $ionicPopup.alert({
                                title: 'Change avatar failed',
                                template: JSON.stringify(err)
                            });
                        });
                }, function(err) {
                    $ionicPopup.alert({
                        title: 'Image picking failure',
                        template: 'Failed on picking an image to change avatar.'
                    });
                });
        };

        $scope.refresh = function() {
            var id = $stateParams.id || AuthService.user._id;

            $scope.prefix = "profile-";
            $scope.notCurrentUser = true;

            if ($state.current.name === "tab.home-profile") {
                $scope.prefix = "home-profile-";
            }

            if ($state.current.name === "tab.discover-profile") {
                $scope.prefix = "discover-profile-";
            }

            if (id === AuthService.user._id) {
                $scope.notCurrentUser = false;
            }

            Profiles.refreshProfile(id)
                .then(function(res) {
                    $rootScope.hide();
                    $scope.profile = res;

                    Profiles.isFollowing(id)
                        .then(function(res) {
                            //
                        }, function(err) {
                            //
                        });

                    Posts.refreshProfile(id)
                        .then(function(res) {
                            $rootScope.hide();
                            $scope.gallery = res;
                            $scope.$broadcast('scroll.refreshComplete');
                        }, function(err) {
                            $rootScope.hide();
                            $scope.$broadcast('scroll.refreshComplete');
                        });
                }, function(err) {
                    $rootScope.hide();
                    $scope.$broadcast('scroll.refreshComplete');
                });
        };

        if (!!AuthService.user) {
            $scope.refresh();
        }
    })

    .controller('FollowerCtrl', function($stateParams, $state, $rootScope, $scope, Profiles, AuthService) {
        $scope.users = [];

        $scope.toggleFollow = function(id) {
            Profiles.toggleFollow(id)
                .then(function(res) {

                }, function(err) {

                });
        };

        $scope.isFollowing = function(id) {
            return !!Profiles.following[id];
        };

        $scope.notCurrentUser = function(id) {
            return AuthService.user._id !== id;
        };

        $scope.refresh = function() {
            var id = $stateParams.id || AuthService.user._id;
            var func;
            $rootScope.show('Please wait...');

            $scope.prefix = "profile-";

            if ($state.current.name === "tab.profile-follower"
                || $state.current.name === "tab.profile-followee") {
                $scope.prefix = "profile-";
            }

            if ($state.current.name === "tab.home-profile-follower"
                || $state.current.name === "tab.home-profile-followee") {
                $scope.prefix = "home-";
            }

            if ($state.current.name === "tab.discover-profile-follower"
                || $state.current.name === "tab.discover-profile-followee") {
                $scope.prefix = "discover-";
            }

            if ($state.current.data.r === "follower") {
                func = Profiles.refreshFollower;
                $scope.title = "Follower";
            }

            if ($state.current.data.r === "followee") {
                func = Profiles.refreshFollowee;
                $scope.title = "Followee";
            }

            func(id)
                .then(function(res) {
                    $rootScope.hide();
                    $scope.users = res;

                    for (var i = 0; i < res.length; i++) {
                        Profiles.isFollowing(res[i]._id)
                            .then(function(res) {
                                //
                            }, function(err) {
                                //
                            });
                    }
                }, function(err) {
                    $rootScope.hide();
                });
        };

        $scope.refresh();
    }) // FollowerCtrl

    .controller('CommentCtrl', function($stateParams, $rootScope, $scope, Comments, API) {
        $rootScope.show('Please wait...');
        $scope.comment = {description: ""};

        $scope.postComment = function() {
            $rootScope.show('Please wait...');

            Comments.postComment($stateParams.id, $scope.comment)
                .then(function(res) {
                    $scope.comment.description = "";
                    $rootScope.hide();
                }, function(err) {
                    $rootScope.hide();
                });
        };

        Comments.refreshComments($stateParams.id)
            .then(function(res) {
                $rootScope.hide();
                $scope.comments = res;
            }, function(err) {
                $rootScope.hide();
            });
    })

    .controller('LoginCtrl', function($rootScope, $state, $scope, $ionicPopup, AuthService, API) {
        $scope.user = {username: "", password: ""};

        $scope.login = function() {
            $rootScope.show('Logging in...');

            AuthService.login($scope.user)
                .then(function(res) {
                    AuthService.user = res.user;
                    $rootScope.hide();
                    $state.go('tab.home', {}, {reload: true});
                }, function(err) {
                    $rootScope.hide();
                    $ionicPopup.alert({
                        title: 'Authenticate Error',
                        template: err.message
                    });
                });
        };

        $scope.register = function() {
            $rootScope.show('Signing Up...');

            AuthService.signup($scope.user)
                .then(function(res) {
                    $rootScope.hide();
                    $ionicPopup.alert({
                        title: 'Signed Up successfully',
                        template: res.message
                    }).then(function() {
                        $scope.login();
                    });
                }, function(err) {
                    $rootScope.hide();
                    $ionicPopup.alert({
                        title: 'Signed Up failed',
                        template: err.message
                    });
                });
        };
    });
